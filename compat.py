# QGIS 2 / 3 compatibility functions

import sys

if sys.version_info >= (3, 0):
    from .compat3 import *

else:
    from qgis.core import QgsMapLayerRegistry, QgsGeometry
    from qgis.core import QgsRuleBasedRendererV2
    from qgis.core import QgsLineSymbolV2

