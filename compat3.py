from qgis.core import QgsProject as QgsMapLayerRegistry

from qgis.core import QgsGeometry as _2QgsGeometry
from qgis.core import QgsPointXY

from qgis.core import QgsRuleBasedRenderer as QgsRuleBasedRendererV2
from qgis.core import QgsLineSymbol as QgsLineSymbolV2

class QgsGeometry(_2QgsGeometry):
    @classmethod
    def fromPoint(cls, pt):
        return cls.fromPointXY(QgsPointXY(pt))
